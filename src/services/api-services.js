/**
* API Services
* Handle API requests, errors
* */
import Bottleneck from 'bottleneck';

const API_BASE = 'https://api.github.com';
const API = {
  SEARCH_REPOSITORIES: `${API_BASE}/search/repositories`,
  SEARCH_ISSUES: `${API_BASE}/repos/`,
};

// Limit API calls to 10 per minute
const limiter = new Bottleneck({
  reservoir: 9, // Max calls in refresh interval
  reservoirRefreshAmount: 9,
  reservoirRefreshInterval: 60000, // One minute
});

function handleErrors(res) {
  if (!res.ok) {
    throw Error(res.status);
  }
  return res;
}

/* ** API Calls ** */

function searchReposByName(query) {
  return limiter.schedule(() => fetch(`${API.SEARCH_REPOSITORIES}${query}`, {
    method: 'GET',
  })
    .then(handleErrors)
    .then(res => res.json())
    .catch(err => err),
  );
}

function getRepoIssues(query) {
  return limiter.schedule(() => fetch(`${API.SEARCH_ISSUES}${query}`, {
    method: 'GET',
  })
    .then(handleErrors)
    .then(res => res.json())
    .catch(err => err),
  );
}

export default {
  searchReposByName,
  getRepoIssues,
};
