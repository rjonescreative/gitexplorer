/**
 * Helper Methods
 * A set of globally accessible methods to kick off api calls,
 * handle results/errors.
 * */
import ApiService from '../services/api-services';

export default {
  mixins: [
    ApiService,
  ],
  methods: {
    handleError(res) {
      if (res instanceof Error) {
        throw Error('SERVER ERROR: Could not complete requested action');
      }
      return res;
    },
    searchReposByName(searchName, page) {
      this.searchInit = true;
      this.loading = true;
      const pageQuery = page > 1 ? `&page=${page}` : '';
      const query = `?q=${encodeURI(searchName)}${pageQuery}`;
      return new Promise((resolve, reject) => {
        ApiService.searchReposByName(query)
          .then(this.handleError)
          .then((res) => {
            this.loading = false;
            resolve(res);
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.log(err);
            reject(err);
          });
      });
    },
    getRepoIssues(searchName, page) {
      const pageQuery = page > 1 ? `&page=${page}` : '';
      const query = `${searchName}/issues?state=open${pageQuery}`;
      return new Promise((resolve, reject) => {
        ApiService.getRepoIssues(query)
          .then(this.handleError)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.log(err);
            reject(err);
          });
      });
    },
  },
};
