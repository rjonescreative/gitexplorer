/**
 * Router
 * Handle application routes
 * */

import Vue from 'vue';
import Router from 'vue-router';
import repoViewer from '@/components/repo-viewer';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'repoViewer',
      component: repoViewer,
    },
  ],
});
